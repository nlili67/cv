
<html>
<head>
    <title>CV</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet">
    <link href='https://fonts.googleapis.com/css?family=Lato:300,400,700' rel='stylesheet' type='text/css'>
</head>
<body>

<div class="container">
    <div class="row">

        <div class="col-lg-8 col-lg-offset-2">

            <h1>CV FORM</h1>


            <!-- We're going to place the form here in the next step -->
            <form id="contact-form" method="post" action="cv.php" role="form" enctype="multipart/form-data">

                <div class="messages"></div>

                <div class="controls">

                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="form_name">Name *</label>
                                <input id="form_name" type="text" name="l" class="form-control" placeholder="Please enter your  name *" required="required" data-error="Name is required.">
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="form_photo">Photo *</label>
                                <input type="file" name="picture" accept="image/*" placeholder="Please enter your stamp size photo *" required="required" data-error="photo is required.">
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="form_email">Email *</label>
                                <input id="form_email" type="email" name="j" class="form-control" placeholder="Please enter your email *" required="required" data-error="Valid email is required.">
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="form_phone">Phone *</label>
                                <input id="form_phone" type="tel" name="i" class="form-control" placeholder="Please enter your phone"  required="required" data-error="Phone number is required.">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="form_name">Father's Name *</label>
                                <input id="form_name" type="text" name="h" class="form-control" placeholder="Please enter your Father's Name *" required="required" data-error="Father's Name  is required.">
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="form_Mother's Name">Mother's Name *</label>
                                <input id="form_Mother's Name" type="text" name="g" class="form-control" placeholder="Please enter your Mother's Name *" required="required" data-error="Mother's Name is required.">
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="form_Age">Age *</label>
                                <input id="form_Age" type="number" name="f" class="form-control" placeholder="Please enter your Age *" required="required" data-error="Age is required.">
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="form_Present Address">Present Address *</label>
                                <input id="form_Present Address" type="text" name="e" class="form-control" placeholder="Please enter your Present Address *" required="required" data-error="Present Address is required.">
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="form_School Name">School Name *</label>
                                <input id="form_School Name" type="text" name="d" class="form-control" placeholder="Please enter your School Name *" required="required" data-error="School Name is required.">
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="form_SSC CGPA">SSC CGPA *</label>
                                <input id="form_SSC CGPA" type="" name="c" class="form-control" placeholder="Please enter your CGPA *" required="required" data-error="SSC CGPA is required.">
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="form_Polytechnic Name">Polytechnic Name*</label>
                                <input id="form_Polytechnic Name" type="text" name="a" class="form-control" placeholder="Please enter your Polytechnic Name *" required="required" data-error="Polytechnic Name is required.">
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>
                        <div class="col-md-6">

                            <div class="form-group">
                                <label for="form_Diploma CGPA">Diploma CGPA *</label>
                                <input id="form_Diploma CGPA" type="" name="b" class="form-control" placeholder="Please enter your CGPA *" required="required" data-error="Diploma CGPA is required.">
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-6">
                        <label for="form_select_gender">Select Gender *</label><br>
                        <form action="">
                            <input type="radio" name="gender" value="male"> Male<br>
                            <input type="radio" name="gender" value="female"> Female<br>
                        </form>
                        </div>

                    </div>

<br>
                    <label for="form_select_gender">Select Skill *</label><br>
                    <div class="form-group">
                        <div class="col-md-6">

                        <label>HTML</label>
                        <input type="checkbox" name="skill[]" value="HTML"><br>
                        <label for="1">CSS</label>
                        <input type="checkbox" name="skill[]" value="CSS"><br>
                        <label for="1">JAVA</label>
                        <input type="checkbox" name="skill[]" value="JAVA"><br>
                        <label for="1">PHP</label>
                        <input type="checkbox" name="skill[]" value="PHP"><br>
                        <label for="1">Java Script</label>
                        <input type="checkbox" name="skill[]" value="Java Script"><br>
                        <label for="1">Python</label>
                        <input type="checkbox" name="skill[]" value="Python"><br>
                        <label for="1">OOP Concept</label>
                        <input type="checkbox" name="skill[]" value="OOP Concept"><br>
                        <label for="1">Microsoft Office</label>
                        <input type="checkbox" name="skill[]" value="Microsoft Office"><br>
                        <label for="1">Mysql</label>
                        <input type="checkbox" name="skill[]" value="Mysql"><br>
                            <label for="1">SEO</label>
                            <input type="checkbox" name="skill[]" value="SEO"><br>
                        </div>
                    </div>
                    <div class="row">

                        <div class="col-md-12">
                            <input type="submit" class="btn btn-success btn-send" value="Submit">
                        </div>
                    </div><br>
                    <div class="row">
                        <div class="col-md-12">
                            <p class="text-muted"><strong>*</strong> These fields are required. Cv form by <a href="http://victoriousbd.blogspot.com" target="_blank">Nilimaa</a>.</p>
                        </div>
                    </div>

                </div>

            </form>

        </div>

    </div>

</div>



<script src="js/jquery.js"></script>
<script src="js/jquery.easing.1.3.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/jquery.fancybox.pack.js"></script>
<script src="js/jquery.fancybox-media.js"></script>
<script src="js/portfolio/jquery.quicksand.js"></script>
<script src="js/portfolio/setting.js"></script>
<script src="js/jquery.flexslider.js"></script>
<script src="js/animate.js"></script>
<script src="js/custom.js"></script>
</body>
</html>

















$objCalculator->add($_POST['num1'], $_POST['num2']);






