<?php

?><!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet">
    <link href='https://fonts.googleapis.com/css?family=Lato:300,400,700' rel='stylesheet' type='text/css'>
    <link href='custom.css' rel='stylesheet' type='text/css'>
</head>
<body>
<div class="container">
    <div class="row">

        <div class="col-sm-6 col-sm-offset-3 text-center">
            <h1>Curriculum Vitae</h1>
            <h3><?php echo $_POST['l']?></h3>
            <h4><?php echo $_POST['i']?></h4>
            <h4><?php echo $_POST['j']?></h4>
        </div>
    </div><br><br>
    <div class="row">
        <h3>Personal Details</h3>

        <?php

        $size=$_FILES['picture']['size'];
        $temporary_info=$_FILES['picture']['tmp_name'];
        $dimension=getimagesize($temporary_info);
        $width=$dimension[0];
        $height=$dimension[1];

        if($size >= (200*1024)){
        if($width/$height==1){
        $picture_name=time().$_FILES['picture']['name'];

        move_uploaded_file($temporary_info,'user_photo/'.$picture_name);
        ?>
        <img src="user_photo/<?php echo $picture_name?>" width="150px"/>
        <?php
        }
        else{
            echo "picture is not in square size";
        }

        }
        else{
            echo "picture size is too large!";
        }
        ?>

        <div class="col-sm-8 col-sm-offset-2">
            <p>Name: <?php echo $_POST['l']?></p>
            <p>Email: <?php echo $_POST['j']?></p>
            <p>Phone Number: <?php echo $_POST['i']?></p>
            <p>Father's Name: <?php echo $_POST['h']?></p>
            <p>Mother's Name: <?php echo $_POST['g']?></p>
            <p>Age: <?php echo $_POST['f']?></p>
            <p>Present Address: <?php echo $_POST['e']?></p>
            <p>Gender: <?php echo $_POST['gender']?></p>
        </div>
    </div><br><br>
    <div class="row">
        <h3>Educational Details</h3>
        <div class="col-sm-8 col-sm-offset-2">
            <table class="table table-bordered">
                <thead>
                <tr>
                    <th>Name Of Exam</th>
                    <th>Institute</th>
                    <th>CGPA</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td>SSC</td>
                    <td><?php echo $_POST['d']?></td>
                    <td><?php echo $_POST['c']?></td>
                </tr>
                <tr>
                    <td>Diploma</td>
                    <td><?php echo $_POST['a']?></td>
                    <td><?php echo $_POST['b']?></td>
                </tr>
                </tbody>
            </table> <br><br>
        </div>
        </div>
    <div>

        <div class="row">
             <h3>Professional Skills</h3>

              <?php
              $a=$_POST['skill'];

              ?>
            <ul>

                <?php
                foreach ($a as $b){
                    echo "<li>$b</li>";
                }
                ?>



            </ul>

</div>

        </div>
    </div>
<script src="js/jquery.js"></script>
<script src="js/jquery.easing.1.3.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/jquery.fancybox.pack.js"></script>
<script src="js/jquery.fancybox-media.js"></script>
<script src="js/portfolio/jquery.quicksand.js"></script>
<script src="js/portfolio/setting.js"></script>
<script src="js/jquery.flexslider.js"></script>
<script src="js/animate.js"></script>
<script src="js/custom.js"></script>
</body>
</html>



